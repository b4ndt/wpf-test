﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using WpfTest01.Business.Model;
using WpfTest01.Business.Service;
using System.Windows.Input;

namespace WpfTest01.Client
{
   public class MainViewModel : INotifyPropertyChanged
   {
      readonly SearchService _searchService;
      ObservableCollection<SearchResult> searchResults;
      ICommand _searchCommand;

      public event PropertyChangedEventHandler PropertyChanged;

      public MainViewModel(SearchService searchService)
      {
         _searchService = searchService;
      }

      public ICommand SearchCommand
      {
         get
         {

            if (_searchCommand == null)
            {
               _searchCommand = new RelayCommand(
                   p => !string.IsNullOrEmpty(SearchTerm),
                   p => Search(SearchTerm));
            }
            return _searchCommand;
         }
      }

      public ObservableCollection<SearchResult> SearchResults
      {
         get { return searchResults; }
         set
         {
            searchResults = value;
            NotifyPropertyChanged();
         }
      }

      private string _searchTerm;

      void Search(string searchTerm)
      {
         SearchResults = new ObservableCollection<SearchResult>(_searchService.Search(searchTerm));
      }

      public string SearchTerm
      {
         get { return _searchTerm; }
         set
         {
            _searchTerm = value;
            NotifyPropertyChanged(nameof(SearchTerm));
         }

      }

      private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
      {
         if (PropertyChanged != null)
         {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
         }
      }
   }
}
