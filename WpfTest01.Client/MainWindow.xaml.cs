﻿using System.Windows;
using WpfTest01.Business.Service;

namespace WpfTest01.Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var  searchService = new SearchService();
            this.DataContext = new MainViewModel(searchService);
        }
    }
}
